@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.product.title'))

@push('after-styles')
	<link href="{{ asset('vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
{{ html()->form('POST', route('admin.category.store'))->class('form-horizontal')->open() }}
	<div class="card">
		<div class="card-header">
			@lang('labels.categories.management')<span class="text-muted"> | @lang('labels.general.create')</span>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="form-group col-sm-4">
					{{ html()->label(__('validation.attributes.name'))->class('form-control-label')->for('name') }}
					{{ html()->text('name')
						->class('form-control')
						->placeholder(__('validation.attributes.name'))
					}}
				</div>
				<div class="form-group col-sm-8">
					{{ html()->label(__('validation.attributes.option'))->class('form-control-label')->for('selOption') }}
					{{ html()->select('options')
						->id('selOption')
						->class('form-control')
						->options($options)
						->multiple('multiple') }}
				</div>
			</div>
		</div>
		<div class="card-footer text-right">
			{{ form_cancel(route('admin.category.index'), '<i class="fas fa-ban"></i> ' . __('buttons.general.cancel'), ['btn btn-danger btn-min']) }}
			{{ form_submit('<i class="far fa-save"></i> ' . __('buttons.general.save'), ['btn btn-success btn-min']) }}
		</div>
	</div>
{{ html()->form()->close() }}
@endsection

@push('after-scripts')
<script src="{{ asset('vendors/select2/js/select2.min.js') }}"></script>
<script type="text/javascript">
	$('#selOption').select2({
		theme:'bootstrap',
		allowClear: true,
		placeholder: '{{ __("validation.attributes.option") }}'
	});
</script>
@endpush