@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="card">
			<div class="card-header">
				<i class="fa fa-edit"></i> @lang('strings.categories.management')
				<div class="float-right">
					<a href="{{ route('admin.category.create') }}" class="btn btn-sm btn-min btn-success"><i class="fas fa-plus"></i> @lang('buttons.general.crud.create')</a>
				</div>
			</div>
			<div class="card-body">
				<table class="table table-striped table-bordered datatable">
					<thead>
						<tr class="text-center">
							<th class="w-2">@lang('labels.categories.tables.numbering')</th>
							<th>@lang('labels.categories.tables.name')</th>
							<th>@lang('labels.categories.tables.option')</th>
							<th class="w-10">@lang('labels.categories.tables.icon')</th>
							<th class="w-2">@lang('labels.categories.tables.status')</th>
							<th>@lang('labels.categories.tables.action')</th>
						</tr>
					</thead>
					<tbody>
						@foreach($categories as $category)
							<tr class="text-center">
								<td>{{ $loop->iteration }}</td>								
								<td class="text-left">{{ $category->name }}</td>
								<td>									
									@foreach($category->options as $option)
										{{ ($loop->iteration > 1 ? ', ' : '') . $option->name }}
									@endforeach
								</td>
								<td>{!! $category->icon_label !!}</td>
								<td>{!! $category->status_label !!}</td>
								<td>{!! $category->action_buttons !!}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection