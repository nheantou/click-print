<!DOCTYPE html>
@langrtl
	<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
	<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title', app_name())</title>
	<meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
	<meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
	<link rel="icon" type="image/svg+xml" href="{{ asset('favicon.svg') }}"/>
	@yield('meta')

	{{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
	@stack('before-styles')

	<!-- Check if the language is set to RTL, so apply the RTL layouts -->
	<!-- Otherwise apply the normal LTR layouts -->
	{{ style(mix('css/backend.css')) }}
	<link href="{{ asset('vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
	<link href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
	<link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
	<link href="{{ asset('css/style.css" rel="stylesheet') }}">
	<link href="{{ asset('vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
	<link href="{{ asset('vendors/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">	
	@stack('after-styles')
	{{ style(mix('css/app.css')) }}
</head>

<body class="{{ config('backend.body_classes') }}">
	@include('backend.includes.header')

	<div class="app-body">
		@include('backend.includes.sidebar')

		<main class="main">
			@include('includes.partials.demo')
			@include('includes.partials.logged-in-as')
			{!! Breadcrumbs::render() !!}

			<div class="container-fluid">
				<div class="animated fadeIn">
					<div class="content-header">
						@yield('page-header')
					</div>

					@include('includes.partials.messages')
					@yield('content')
				</div>
			</div>
		</main>

		@include('backend.includes.aside')
	</div>

	@include('backend.includes.footer')
	<script src="{{ asset('vendors/jquery/js/jquery.min.js') }}"></script>
	<script src="{{ asset('vendors/popper.js/js/popper.min.js') }}"></script>
	<script src="{{ asset('vendors/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('vendors/pace-progress/js/pace.min.js') }}"></script>
	<script src="{{ asset('vendors/perfect-scrollbar/js/perfect-scrollbar.min.js') }}"></script>
	<script src="{{ asset('vendors/@coreui/coreui-pro/js/coreui.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net-bs4/js/dataTables.bootstrap4.js') }}"></script>
	<script src="{{ asset('js/datatables.js') }}"></script>

	@stack('after-scripts')
</body>
</html>
