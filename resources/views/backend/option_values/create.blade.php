@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.product.title'))

@section('content')
{{ html()->form('POST', route('admin.option.value.store', $optionId))->class('form-horizontal')->open() }}
	<div class="card">
		<div class="card-header">
			@lang('labels.option.management')<span class="text-muted"> | @lang('labels.general.create')</span>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="form-group col-sm-4">
					{{ html()->label(__('validation.attributes.name'))->class('form-control-label')->for('name') }}
					{{ html()->text('name')
						->class('form-control')
						->placeholder(__('validation.attributes.name'))
					}}
				</div>
			</div>
		</div>
		<div class="card-footer text-right">
			{{ form_cancel(route('admin.option.value.index', $optionId), '<i class="fas fa-ban"></i> ' . __('buttons.general.cancel'), ['btn btn-danger btn-min']) }}
			{{ form_submit('<i class="far fa-save"></i> ' . __('buttons.general.save'), ['btn btn-success btn-min']) }}
		</div>
	</div>
{{ html()->form()->close() }}
@endsection