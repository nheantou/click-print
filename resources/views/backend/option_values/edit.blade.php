@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
{{ html()->modelForm($optionValue, 'PATCH', route('admin.option.value.update', [$optionId, $optionValue->id]))->class('form-horizontal')->open() }}
	<div class="card">
		<div class="card-header">
			@lang('labels.categories.management')<span class="text-muted"> | @lang('labels.general.edit')</span>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="form-group col-sm-4">
					{{ html()->label(__('validation.attributes.name'))->class('form-control-label')->for('name') }}
					{{ html()->text('name')
						->class('form-control')
						->placeholder(__('validation.attributes.name'))
						->attribute('maxlength', 191)
						->required()
						->autofocus() }}
				</div>
			</div>
		</div>
		<div class="card-footer text-right">
			{{ form_cancel(route('admin.option.value.index', $optionId), '<i class="fas fa-ban"></i> ' . __('buttons.general.cancel'), ['btn btn-danger btn-min']) }}
			{{ form_submit('<i class="far fa-save"></i> ' . __('buttons.general.save'), ['btn btn-success btn-min']) }}
		</div>
	</div>
{{ html()->closeModelForm() }}
@endsection