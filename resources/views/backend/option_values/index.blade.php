@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="card">
	<div class="card-header">
		<i class="fa fa-edit"></i> @lang('strings.options.management')
		<div class="float-right">
			<a href="{{ route('admin.option.value.create', $optionId) }}" class="btn btn-sm btn-min btn-success"><i class="fas fa-plus"></i> @lang('buttons.general.crud.create')</a>
		</div>
	</div>
	<div class="card-body">
		<table class="table table-striped table-bordered datatable">
			<thead>
				<tr class="text-center">
					<th class="w-2">@lang('labels.options.tables.numbering')</th>
					<th class="text-left">@lang('labels.options.tables.name')</th>
					<th>@lang('labels.options.tables.status')</th>
					<th>@lang('labels.options.tables.action')</th>
				</tr>
			</thead>
			<tbody>
				@foreach($optionValues as $value)
					<tr class="text-center">
						<td>{{ $loop->iteration }}</td>
						<td class="text-left">{{ $value->name }}</td>
						<td>{!! $value->status_label !!}</td>
						<td>{!! $value->action_buttons !!}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection
