<footer class="app-footer">
	<div>
		<strong>
			@lang('strings.general.copyright', ['date' => date('Y')]) <a href="#">Daily Solution</a>
		</strong>
	</div>

	<div class="ml-auto">Version 0.1.2</div>
</footer>
