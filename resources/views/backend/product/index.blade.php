@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<div class="card">
	<div class="card-header">
		<i class="fa fa-edit"></i> @lang('strings.products.management')
		<div class="float-right">
			<a href="{{ route('admin.product.create') }}" class="btn btn-sm btn-success"><i class="fas fa-plus"></i> @lang('buttons.general.crud.create')</a>
		</div>
	</div>
	<div class="card-body">
		<table class="table table-striped table-bordered datatable">
			<thead>
				<tr class="text-center">
					<th>@lang('labels.products.tables.numbering')</th>
					<th>@lang('labels.products.tables.code')</th>
					<th class="text-left">@lang('labels.products.tables.name')</th>
					<th class="text-left">@lang('labels.products.tables.category')</th>
					<th class="text-right">@lang('labels.products.tables.price')</th>
					<th>@lang('labels.products.tables.status')</th>
					<th>@lang('labels.products.tables.action')</th>
				</tr>
			</thead>
			<tbody>
				@foreach($products as $product)
					<tr class="text-center">
						<td>{{ $loop->iteration }}</td>
						<td>{{ $product->code }}</td>
						<td class="text-left">{{ $product->name }}</td>
						<td class="text-left">{{ $product->category->name }}</td>
						<td class="text-right">{{ $product->price_label }}</td>
						<td>{!! $product->status_label !!}</td>
						<td>{!! $product->action_buttons !!}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection

@push('after-scripts')
<script type="text/javascript">
	$(function () {
		$('#category').change(function () {
			{{-- ajax action --}}
			$.ajax({
				type: 'GET',
				url: '{{ url("admin/category") }}' + '/' + $(this).val() + '/get',
				success: function (res) {
					$('#type').html(res.html);
				}
			});
		});
	});
</script>
@endpush()
