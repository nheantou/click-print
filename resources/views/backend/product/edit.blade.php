@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
{{ html()->modelForm($product, 'PATCH', route('admin.product.update', $product->id))->class('form-horizontal')->open() }}
<div class="card">
	<div class="card-header">
		@lang('strings.products.management')<span class="text-muted"> | @lang('labels.general.edit')</span>
	</div>
	<div class="card-body">
		<div class="row">
			<div class="form-group col-sm-3">
				{{ html()->label('Code')->class('form-control-label')->for('code') }}
				{{ html()->text('code')
					->class('form-control')
				}}
			</div>
			<div class="form-group col-sm-5">
				{{ html()->label('Name')->class('form-control-label')->for('name') }}
				{{ html()->text('name')
					->class('form-control')
				}}
			</div>
			<div class="form-group col-sm-4">
				<label for="company">Image</label><br>
				<input id="file-input" type="file" name="file-input">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-3">
				{{ html()->label('Category')->class('form-control-label')->for('category') }}
				{{ html()->select('category')
					->class('form-control')
					->options($categories)
					->value($product->category_id)
				}}
			</div>

			<div class="form-group col-sm-9">
				<div id="optionValue">@include('backend.options.value')</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-sm-3">
				{{ html()->label('Price')->class('form-control-label')->for('price') }}
				{{ html()->text('price')
					->class('form-control')
				}}
			</div>
		</div>
	</div>
	<div class="card-footer text-right">
		{{ form_cancel(route('admin.product.index'), '<i class="fas fa-ban"></i> ' . __('buttons.general.cancel'), ['btn btn-danger btn-min']) }}
		{{ form_submit('<i class="far fa-save"></i> ' . __('buttons.general.save'), ['btn btn-success btn-min']) }}
	</div>
</div>
{{ html()->closeModelForm() }}


@endsection