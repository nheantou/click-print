@extends('frontend.layouts.app')

@section('title', 'sell')
<style>
    .height-img {
        height: 175px;
    }

    .height-product{
        height: 78px;
        margin-top: 5;
        width: 83px;
        word-wrap: break-word !important;
        white-space: normal !important;
    }

    .button-sort {
      /* height: 45px; */
      margin-top: 5;
      width: 100px;
    }
</style>

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <p>hello sale</p>
                    {{-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                          <div class="carousel-item active">
                              <img src="{{asset('img/frontend/img/slice-1.jpg') }}" class="d-block w-100 height-img" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="{{asset('img/frontend/img/slice-2.jpg') }}" class="d-block w-100 height-img" alt="...">
                          </div>
                          <div class="carousel-item">
                            <img src="{{asset('img/frontend/img/slice-3.jpg') }}" class="d-block w-100 height-img" alt="...">
                          </div>
                        </div>
                    </div> --}}
                </div><!--card-header-->

                <div class="card-body">
                    <div class="row">
                        <div class="col col-sm-5 order-1 order-sm-2  mb-4">
                            <div class="table-responsive">
                              <table class="table">
                                  <thead class="thead-dark">
                                    <tr>
                                      <th scope="col">ក្រដាស</th>
                                      <th scope="col">ក្រាម</th>
                                      <th scope="col">តម្លៃ</th>
                                      <th scope="col">ចំនួន</th>
                                      <th scope="col">សរុប</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td scope="row">A4/BW</td>
                                      <td>80g</td>
                                      <td>100</td>
                                      <td>50</td>
                                      <td>5000</td>
                                    </tr>
                                    <tr>
                                      <td scope="row">A5/Color</td>
                                      <td>100g</td>
                                      <td>200</td>
                                      <td>20</td>
                                      <td>8000</td>
                                    </tr>
                                  </tbody>
                                  {{-- <tfoot>
                                      <tr>
                                          <th scope="row">2</th>
                                          <th>Jacob</th>
                                          <th>Thornton</th>
                                          <th>@fat</th>
                                      </tr>
                                    </tfoot> --}}
                              </table>
                            </div>
                            <form action="#">
                              <button type="submit" class="btn btn-success">គិតលុយ</button>
                              <button type="submit" class="btn btn-success">ចេញវិក័យប័ត្រ</button>
                            </form>
                            {{-- <button class="w3-button w3-white w3-border w3-border-blue w3-round-large">គិតលុយ</button> --}}
                        </div><!--col-md-4-->

                        <div class="col-md-7 order-2 order-sm-1">
                            {{-- <div class="w3-panel w3-border-top w3-border-bottom w3-border-green"> --}}
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">A3</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">A4</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">A5</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                                <button class="w3-button w3-white w3-border w3-border-red w3-round-large height-product">Button</button>
                            {{-- </div> --}}
                            <hr>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">A4/BW</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">A4/Color</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">A4/BW2</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">A4/Color2</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">Button</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">Button</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">Button</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">Button</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large height-product">Button</button>
                            <hr>
                            <form action="#">
                              <button type="submit" class="btn btn-dark button-sort">BW</button>
                              <button type="submit" class="btn btn-danger button-sort">COLOR</button>
                              <button type="submit" class="btn btn-dark button-sort">BW*2</button>
                              <button type="submit" class="btn btn-danger button-sort">COLOR*2</button>
                              <button type="submit" class="btn btn-info button-sort">Customer</button>
                            </form>
                            {{-- <button class="w3-button w3-white w3-border w3-border-black w3-round-large button-sort">BW</button>
                            <button class="w3-button w3-white w3-border w3-border-red w3-round-large button-sort">COLOR</button>
                            <button class="w3-button w3-white w3-border w3-border-black w3-round-large button-sort">BW*2</button>
                            <button class="w3-button w3-white w3-border w3-border-red w3-round-large button-sort">COLOR*2</button>
                            <button class="w3-button w3-white w3-border w3-border-blue w3-round-large">By Customer</button> --}}
                        </div><!--col-md-8-->
                    </div><!-- row -->
                </div> <!-- card-body -->
            </div><!-- card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
