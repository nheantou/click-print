@if(!empty($products))
	<div class="row">
		@foreach($products as $product)
			<div data-value="{{ $product->id }}" class="proItem col-6 col-sm-6 col-lg-3">
				<div class="card">
					<div class="card-body p-2">
						<h6 class="proName text-danger">{{ $product->name }}</h6>
						<div class="float-left">
							<i class="fa fa-sticky-note bg-danger p-3 font-2xl mr-2"></i>
						</div>
						<div class="float-right">
							<table class="table table-striped m-0">
								<tbody>
									@foreach($product->option_values as $value)
										<tr>
											<td class="p-0 py-2 pr-2 text-right">
												<small>{{ $value->option->name }}</small>
											</td>
											<td class="p-0 py-2 text-right">
												{{ $value->name }}
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>

					<div class="card-footer px-3 py-2">
						<div class="btn-block d-flex justify-content-between align-items-center">
							<span class="small font-weight-bold">
								R. <span class="proPrice">{{ $product->price }}</span>
							</span>
							<i class="fa fa-angle-right"></i>
						</div>
					</div>
				</div>
			</div>
		@endforeach
	</div>
@endif