@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )
{{-- <script>
    setInterval(myTimer, 1000);
    function myTimer() {
        var date = new Date();
        var time = date.toLocaleTimeString();
        $("#timer").text(time);
    }
</script> --}}
{{-- <style>

</style> --}}

@section('content')
<div class="container">
<div class="row">
    <div class="col-12">
        <div class="col-xl-12">
        <div class="card text-white bg-light">
            <div class="card-body pb-0">
                <div class="row mb-2">
                    <div class="col-6 text-center">
                        <img class="img-fluid w-50" src="{{ asset('img/logo.png') }}">
                    </div>
                    <div class="col-6 text-center text-dark">
                        <div class="card">
                            <div class="card-header bg-dark"><label for="" id="timer"></label></div>
                            <div class="card-body">
                                <span id="text-welcome">WELCOME TO CLICK PRINT</span>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="row">
                            <div class="col-4">
                                    <div class="card text-white bg-primary">
                                        <div class="card-body">
                                            <div class="h1 text-muted text-right">
                                            <i class="fa fa-cart-plus fa-lg"></i>
                                                </div>
                                            <div class="text-value text-center">POS</div>
                                            <div class="text-center">Click here to sell products</div>
                                                <div class="progress progress-white progress-xs my-2">
                                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                                {{-- <small class="text-muted">Lorem ipsum dolor sit amet enim.</small> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card text-white bg-danger">
                                    <div class="card-body">
                                    <div class="h1 text-muted text-right">
                                        <i class="fa fa-television fa-lg"></i>
                                    </div>
                                        <div class="text-value text-center">CLICK PRINT</div>
                                        <div class="text-center">Click here to admin</div>
                                            <div class="progress progress-white progress-xs my-2">
                                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="card text-white bg-success">
                                    <div class="card-body">
                                    <div class="h1 text-muted text-right">
                                            <i class="fa fa-bar-chart fa-lg"></i>
                                    </div>
                                        <div class="text-value text-center">REPORTS</div>
                                        <div class="text-center">Click here to your sale reports</div>
                                            <div class="progress progress-white progress-xs my-2">
                                            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                    <div class="card text-white bg-yellow">
                                        <div class="card-body">
                                        <div class="h1 text-muted text-right">
                                            <i class="fa fa-th-list fa-lg"></i>
                                        </div>
                                            <div class="text-value text-center">SERVICES</div>
                                            <div class="text-center">Click here to services details</div>
                                                <div class="progress progress-white progress-xs my-2">
                                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4">
                                        <div class="card text-white bg-cyan">
                                            <div class="card-body">
                                                <div class="h1 text-muted text-right">
                                                    <i class="fas fa-users fa-lg"></i>
                                                </div>
                                                <div class="text-value text-center">CUSTOMERS</div>
                                                <div class="text-center">Click here to customers</div>
                                                    <div class="progress progress-white progress-xs my-2">
                                                        <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-4">
                                <a href="{{route('frontend.sale.invoice')}}">
                                    <div class="card text-white bg-success">
                                        <div class="card-body">
                                        <div class="h1 text-muted text-right">
                                            <i class="fa fa-signal fa-lg"></i>
                                        </div>
                                            <div class="text-value text-center">INVOICES</div>
                                            <div class="text-center">Click here to your invoices</div>
                                            <div class="progress progress-white progress-xs my-2">
                                                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>
</div>
@endsection
