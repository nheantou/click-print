@extends('frontend.layouts.app')

@section('title', 'sell')
<style>
.size-btn-money{
    width: 83px;
}
</style>
@section('content')

      <div class="row justify-content-center align-items-center">
            <div class="col-sm-6 align-self-center">
                <div class="card">
                    <div class="card-header">
                        <strong>
                            {{-- @lang('labels.frontend.auth.register_box_title') --}}
                            Payment
                        </strong>
                    </div><!--card-header-->
    
                    <div class="card-body">
                        {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">កាលបរិច្ឆេទ</label>
                                        <div class="col-sm-8">
                                            <input type="date" class="form-control" id="inputEmail3" placeholder="កាលបរិច្ឆេទ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">អតិថិជន</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="អតិថិជន">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">ប្រាក់សរុប</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="លុយសរុប">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">បញ្ចុះតម្លៃ</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="បញ្ចុះតម្លៃ">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">ប្រាក់ត្រូវបង់</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="ប្រាក់ត្រូវបង់">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">ប្រាក់ទទួល</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="ប្រាក់ទទួល">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">ប្រាក់អាប់</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="inputEmail3" placeholder="ប្រាក់អាប់">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                            <button type="button" class="btn btn-success btn-lg">Print</button>
                                            <button type="button" class="btn btn-danger btn-lg">Close</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    {{ html()->button('500៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg size-btn-money')}}
                                                </div>
                                            </div>
                                            <div class="col-sm-4">   
                                                <div class="form-group text-center">
                                                    {{ html()->button('1000៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg size-btn-money')}}
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    {{ html()->button('2000៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg size-btn-money')}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    {{ html()->button('5000៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg size-btn-money')}}
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    {{ html()->button('10000៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg')}}
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group text-center">
                                                    {{ html()->button('20000៛')
                                                        ->value(1)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-lg')}}
                                                </div>                                               
                                            </div>
                                        </div>





                                        <div class="row">
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('1')
                                                            ->value(1)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('2')
                                                            ->value(2)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('3')
                                                            ->value(3)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('4')
                                                            ->value(4)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('5')
                                                            ->value(5)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('6')
                                                            ->value(6)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('7')
                                                            ->value(7)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                        {{ html()->button('8')
                                                            ->value(8)
                                                            ->type('button')
                                                            ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                    {{ html()->button('9')
                                                        ->value(9)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                    {{ html()->button('<i class="fas fa-times"></i>')
                                                        ->id('btnClear')
                                                        ->type('button')
                                                        ->class('btn btn-danger btn-circle btn-lg')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                    {{ html()->button('0')
                                                        ->value(0)
                                                        ->type('button')
                                                        ->class('btn btn-primary btn-circle btn-lg btn-number')}}
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group text-center">
                                                    {{ html()->button('<i class="fas fa-arrow-left"></i>')
                                                        ->id('btnLogin')
                                                        ->type('button')
                                                        ->class('btn btn-success btn-circle btn-lg')}}
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                            </div><!--row-->
                    </div><!-- card-body -->
                </div><!-- card -->
            </div><!-- col-md-8 -->
        </div><!-- row -->
@endsection
