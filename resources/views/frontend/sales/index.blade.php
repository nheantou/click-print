@extends('frontend.layouts.app')

@section('title', 'DS - POS')

@section('content')
<div class="row">
	<div class="col-sm-12 col-md-7">
		{{ html()->form('GET', route('frontend.product.search'))->id('frmSearch')->class('form-horizontal')->open() }}
			<div class="card">
				<div class="card-header bg-dark">
					@foreach($categories as $category)
						<button data-value="{{ $category->id }}" data-remote="{{ route('frontend.category.option', $category->id) }}" class="btnCategory{{ $loop->iteration == 1 ? ' catCurrent' :  '' }} btn btn-outline-primary mr-2 {{ ($loop->first ? 'btn-primary text-white' : '') }}" type="button"><i class="{{ $category->icon }}"></i> {{ $category->name }}</button>
					@endforeach
					{{ html()->select('category')
						->id('category')
						->attribute('hidden')
						->options($categories->pluck('name', 'id')) }}
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-sm-12">
							<div id="optionFilters"></div>
						</div>
					</div>
					<hr class="mt-2" />

					<div id="proContent"></div>
				</div>
			</div>
		{{ html()->form()->close() }}
	</div>

	<div class="col-sm-12 col-md-5">
		<div class="card">
			<div class="card-header py-3">
				Customer name
			</div>
			<div class="card-body h-body">
				<table id="itemTable" class="table table-responsive-sm table-bordered table-striped table-sm">
					<thead>
						<tr>
							<th class="w-25 text-center">Qty</th>
							<th>Items</th>
							<th>Price</th>
							<th>Subtotal</th>
							<th class="text-center">Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>				
			</div>

			<div class="card-footer">
				<div class="row">
					<table class="table table-responsive-sm table-striped table-sm">
						<thead>
							<tr>
								<th class="text-right w-25">Total items:</th>
								<th class="bg-primary"><span id="totalItem">0</span></th>
								<th class="text-right">Total:</th>
								<th class="bg-primary">R. <span class="totalPrice">0</span></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-right">DISCOUNT($)</td>
								<td><input type="number" class="btn btn-block btn-ghost-dark active btn-sm btn-pill"></td>
								<td class="text-right">TAX (%)</td>
								<td><input type="number" class="btn btn-block btn-ghost-dark active btn-sm btn-pill"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="fixed-bottom">
		<div class="bg-dark py-2">
			<div class="row">
				<div class="col-sm-5 text-center">
					<div class="m-auto font-weight-bolder font-5xl">R. <span class="totalPrice">0</span></div>
				</div>
				<div class="col-sm-2">
					<button class="btn btn-danger btn-lg btn-block py-3"><i class="fa fa-money"></i> PAY NOW</button>
				</div>
				<!-- <div class="col-sm-2">
					<button class="btn btn-danger btn-lg btn-block py-3"><i class="fa fa-money"></i> PAY NOW</button>
				</div>
				<div class="col-sm-2">
					<button class="btn btn-success btn-lg btn-block py-3"><i class="fa fa-money"></i> PAY NOW</button>
				</div> -->
			</div>
		</div>
	</div>
	<!-- <div class="fixed-bottom">
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<div class="navbar-nav w-75 text-center">
				<div class="m-auto font-weight-bolder font-5xl">R. <span class="totalPrice">0</span></div>
			</div>
			<div class="w-25">
				<button class="btn btn-danger btn-lg btn-block py-3"><i class="fa fa-money"></i> PAY NOW</button>
			</div>
			<div class="w-25 mx-1">
				<button class="btn btn-success btn-lg btn-block py-3"><i class="fa fa-money"></i> HOLD</button>
			</div>
		</nav>
	</div> -->
</div>
@endsection

@push('after-scripts')
<script type="text/javascript">
	$(function() {
		var _totalAmount = 0;

		{{-- auto action load option when first category --}}
		__actionCatClick($('.btnCategory').eq(0));

		$('.btnCategory').click(function () {
			$('.btnCategory').removeClass('btn-primary text-white');
			$(this).addClass('btn-primary text-white');

			__actionCatClick($(this));
		});

		{{-- action change option --}}
		$(document).on('change', '.selOptions', function () {
			__actionSearch();

			console.log($('#frmSearch').serialize());
		});

		{{-- click product added to table invoice --}}
		$(document).on('click', '.proItem', function () {
			{{-- if has row item, be added--}}
			if(__checkRowItem($(this), $('.rowItem')) === 0) {
				__addRowItem($('#itemTable tbody'), $(this));

				{{-- updated total price --}}
				__updateTotalPrice();
			}

			{{-- updated total item and price --}}
			__updateTotalItem();
		});

		{{-- change qty table invoice --}}
		$(document).on('change', '.qty', function () {
			var _parent = $(this).parents('.rowItem');
			var _unitPrice = _parent.find('.unitPrice').text();

			{{-- updated subTotal --}}
			_parent.find('.subTotal').text(__calcSubTotal(_unitPrice, $(this).val()));

			{{-- updated total price --}}
			__updateTotalPrice();
		});

		$(document).on('click', '.btnRemove', function () {
			$(this).parents('.rowItem').remove();

			__updateTotalItem();
			__updateTotalPrice();
		});

		function __actionCatClick(_this) {
			$('#category').val(_this.data('value'));

			{{-- ajax action --}}
			$.ajax({
				type: 'GET',
				url: _this.data('remote'),
				success: function (res) {
					$('#optionFilters').html(res.html);

					{{-- get all products --}}
					__actionSearch();
				}
			});
		}

		function __actionSearch() {
			$.ajax({
				type: $('#frmSearch').attr('method'),
				url: '{{ route("frontend.product.search") }}',
				data: $('#frmSearch').serialize(),
				success: function (res) {
					$('#proContent').html(res.html);

					console.log(res);
				}
			});
		}

		function __calcSubTotal(unitPrice = 0, qty = 0) {
			return (unitPrice * qty);
		}

		function __checkRowItem(_this, el) {
			var _check = 0;

			if (el.length) {
				el.each(function (index) {
					{{-- if has that row update and break from loop --}}
					{{-- $(this) is el itemTable --}}
					if ($(this).data('value') === _this.data('value')) {

						{{-- update and event change qty --}}
						var _qty = parseInt($(this).find('.qty').val()) + 1;
						$(this).find('.qty').val(_qty);
						$(this).find('.qty').change();

						_check = 1;
						return false;
					}
				});
			}

			return _check;
		}

		function __addRowItem(el, _this) {
			var _proPrice = _this.find('.proPrice').text();
			var _row = '<tr data-value="' + _this.data('value') + '" class="rowItem">' + 
					'<td class="text-center"><input type="number" class="qty btn btn-ghost-dark active btn-sm btn-pill" value="1"></td>' +
					'<td>' + _this.find('.proName').text()  + '</td>' +
					'<td>R. <span class="unitPrice">' + _proPrice  + '<span></td>' +
					'<td>R. <span class="subTotal">' + __calcSubTotal(_proPrice, 1) + '</span></td>' +
					'<td class="text-center"><button type="button" class="btnRemove btn btn-sm btn-danger"><i class="fa fa-trash-o"></i></btton></td>' +
					'</tr>';

			el.append(_row);
		}

		function __updateTotalItem() {
			$('#totalItem').text($('.rowItem').length);
		}

		function __updateTotalPrice() {
			_totalAmount = 0;
			$('.subTotal').each(function () {
				_totalAmount += parseInt($(this).text());
			});			

			$('.totalPrice').text(_totalAmount);
		}
	});
</script>
@endpush
