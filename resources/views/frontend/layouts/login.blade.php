<!DOCTYPE html>
@langrtl
	<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
	<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>@yield('title', app_name())</title>
		<meta name="description" content="Daily Solution">
		<meta name="author" content="Daily Solution">
		<link rel="icon" type="image/svg+xml" href="{{ asset('favicon.svg') }}"/>
		@yield('meta')

		{{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
		@stack('before-styles')

		<!-- Check if the language is set to RTL, so apply the RTL layouts -->
		<!-- Otherwise apply the normal LTR layouts -->
		{{ style(mix('css/frontend.css')) }}
		{{ style(mix('css/custom.css')) }}
        {{ style(mix('css/w3css.css')) }}
        <!-- Icons-->
        <link href="{{ asset('vendors/@coreui/icons/css/coreui-icons.min.css') }}" rel="stylesheet">
		<link href="{{ asset('vendors/flag-icon-css/css/flag-icon.min.css') }}" rel="stylesheet">
		<link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
		<link href="{{ asset('vendors/simple-line-icons/css/simple-line-icons.css') }}" rel="stylesheet">
		<!-- Main styles for this application-->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
		<link href="{{ asset('vendors/pace-progress/css/pace.min.css') }}" rel="stylesheet">
		@stack('after-styles')
	</head>
	<body class="layout-login">
		@include('includes.partials.demo')

		<div id="app">
			<div class="app flex-row align-items-center">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-sm-5">

					</div>
				</div>

				@yield('content')
			</div>
			</div>
		</div>

		@stack('before-scripts')
		{!! script(mix('js/manifest.js')) !!}
		{!! script(mix('js/vendor.js')) !!}
		{!! script(mix('js/frontend.js')) !!}
		@stack('after-scripts')

		@include('includes.partials.ga')
	</body>
</html>
