<div class="row">
	@foreach ($options as $option)
		<div class="form-group col-sm-3">
			{{ html()->label($option->name)->class('form-control-label')->for('options[' . $option->id . ']') }}
			{{ html()->select('options[' . $option->id . ']')
				->class('form-control selOptions')
				->attribute(!$option->values->count() ? 'disabled' : '')
				->options($option->values->pluck('name', 'id'))
				->value(!empty($values[$option->id]) ? $values[$option->id] : '')
			}}
		</div>
	@endforeach
</div>