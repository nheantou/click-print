@extends('frontend.layouts.login')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
<div class="row justify-content-center">
      <div class="col-md-8">
      @include('includes.partials.messages')
        <div class="card-group py-5">
          <div class="card">
            <div class="card-body">
            {{ html()->form('POST', route('frontend.auth.admin.login.post'))->open() }}
              <h1>Login</h1>
              <p class="text-muted">Sign In to your account</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-user"></i>
                  </span>
                </div>
                {{ html()->email('email')
							->class('form-control')
							->placeholder(__('validation.attributes.frontend.email'))
							->attribute('maxlength', 191)
							->required() }}
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                {{ html()->password('password')
							->class('form-control')
							->placeholder(__('validation.attributes.frontend.password'))
							->required() }}
              </div>
              <div class="row">
                <div class="col-6">
                {{ form_submit(__('labels.frontend.auth.login_button'), 'btn btn-primary px-4') }}
                </div>
              </div>
            {{ html()->form()->close() }}
            </div>
          </div>
          <div class="card text-white py-5 d-md-down-none" style="width:44%; background-color:#f0f3f5;">
            <div class="card-body text-center">
              <div class="py-3">
                <img class="img-fluid" src="{{ asset('img/logo.png') }}">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection





