@extends('frontend.layouts.login')

@section('content')
<div class="row justify-content-center">
	<div class="col-sm-4 py-5">
	@include('includes.partials.messages')
		<div class="card">
			<div class="card-body">
				{{ html()->form('POST', route('frontend.auth.login.post'))->id('frmLogin')->open() }}
					<div class="row ">
						<div class="col">
							<div class="form-group py-3">
								<div class="blockText blockText0">
									<h6 class="text-primary text-center">Enter Your ID</h6>
									{{ html()->text('txtid')
										->id('textid')
										->class('text-center form-control f50 py-3 btn-pill')
										->attribute('maxlength', 4)
										{{-- ->attribute('onkeypress', 'return isNumberKey(event)') --}}
										->autofocus()
										->required() }}
								</div>
								<div class="blockText blockText1 dnone">
									<h6 class="text-primary text-center">Enter Your Pin</h6>
									{{ html()->password('txtpassword')
										->id('textpin')
										->class('text-center form-control btn-pill f50 py-3')
										->attribute('maxlength', 4)
										->autofocus()
										->required() }}
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('1')
								->value(1)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('2')
								->value(2)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('3')
								->value(3)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('4')
								->value(4)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('5')
								->value(5)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('6')
								->value(6)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('7')
								->value(7)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('8')
								->value(8)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('9')
								->value(9)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
					</div>
						  <div class="row">
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('<i class="fas fa-times"></i>')
								->id('btnClear')
								->type('button')
								->class('btn btn-danger btn-circle btn-lg')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('0')
								->value(0)
								->type('button')
								->class('btn btn-primary btn-circle btn-lg btn-number')}}
							</div>
						</div>
						<div class="col">
							<div class="form-group text-center">
							{{ html()->button('<i class="fas fa-arrow-right"></i>')
								->id('btnLogin')
								->type('button')
								->class('btn btn-success btn-circle btn-lg')}}
							</div>
						</div>
					</div>
				{{ html()->form()->close() }}
			</div>
		</div>
	</div>
</div>
@endsection

@push('after-scripts')
<script type="text/javascript">
	// function isNumberKey(evt) {
	// 	var charCode = (evt.which) ? evt.which : evt.keyCode;
	// 	console.log(charCode);
	// 	if (charCode != 46 && charCode > 31  && (charCode < 48 || charCode > 57)) return false;

	// 	return true;
	// }

	$(function() {
		var input = 'textid';
		var txtValue = '';
		var blockText = 0;

		$('.btn-number').click(function() {
			addValue($('#' + input), $(this));
		});

		$('#' + input).keypress(function () {
			addValue($('#' + input), $(this));
		});

		$('#btnClear').click(function() {
			txtValue = '';
			$('#' + input).val(txtValue);
		});

		{{-- button login --}}
		$('#btnLogin').click(function() {

			if (blockText == 1) {
				$('#frmLogin').submit();

				$(this).attr('disabled', true);
				return true;
			}

			input = 'textpin';
			txtValue = '';

			$('.blockText').hide();

			blockText = 1 - blockText;
			$('.blockText' + blockText).show();
		});

		function addValue(el, _this) {
			if (el.val().length < 4) {
				txtValue = txtValue.concat(_this.val());
				el.val(txtValue);

				console.log(txtValue);
			}
		}
	});
</script>
@endpush
