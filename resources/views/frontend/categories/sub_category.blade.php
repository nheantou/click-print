@if(!empty($subCategories))
	@foreach($subCategories as $sub)
		<button data-remote="{{ route('frontend.category.product', $sub->id) }}" class="subCategory btn btn-lg mr-2 btn-outline-primary {{ $loop->first ? 'btn-primary text-white' : ''}}">{{ $sub->name }}</button>
	@endforeach
@endif