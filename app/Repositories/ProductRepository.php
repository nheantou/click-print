<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Product\Product;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Product::class;
	}
	
	public function getAllProducts()
	{
		return $this->model			
			->get();
	}

	public function getProductByFilter(array $data)
	{
		$query = $this->model;

		$query = $query->where('category_id', $data['category']);

		// $options = $data['options'];
		// $query = $query->whereHas('option_values', function ($query) use ($options) {
		// 	$query->where('option_value_id', $options[1])
		// 		->where('option_value_id', $options[3]);
		// });

		// filter options
		if (!isset($data['options']) || ! count($data['options'])) {
			$options = $data['options'];
			$query = $query->whereHas('option_values', function ($query) use ($options) {
				// foreach ($options as $key => $value) {
				// 	$q->where('option_value_id', $value);
				// }
				$query->whereIn('option_value_id', $options);
			});
		}

		return $query->get();
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return Product
	 */
	public function create(array $data) : Product
	{
		return DB::transaction(function () use ($data) {
			$product = parent::create([
				'code'			=> $data['code'],
				'name'			=> $data['name'],
				'category_id'	=> $data['category'],
				'price'			=> $data['price'],
			]);

			// if adding any option value
			if (!isset($data['options']) || ! count($data['options'])) {
				$data['options'] = [];
			}

			if ($product) {

				// adding option value
				$product->option_values()->sync($data['options']);

				return $product;
			}

			throw new GeneralException(__('exceptions.products.create_error'));
		});
	}

	/**
	 * @param Product $product
	 * @param array $data
	 *
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Product $product, array $data) : Product
	{
		// if adding any option value
		if (!isset($data['options']) || ! count($data['options'])) {
			$data['options'] = [];
		}

		return DB::transaction(function () use ($product, $data) {
			if ($product->update([
				'code'				=> $data['code'],
				'name'				=> $data['name'],
				'category_id'		=> $data['category'],
				'price'				=> $data['price']
			])) {
				
				// update option value
				$product->option_values()->sync($data['options']);

				return $product;
			}

			throw new GeneralException(__('exceptions.products.update_error'));
		});
	}
}
