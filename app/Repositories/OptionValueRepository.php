<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\OptionValue\OptionValue;

/**
 * Class OptionValueRepository.
 */
class OptionValueRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return OptionValue::class;
	}

	public function getOptionValues($option=0)
	{
		return $this->model
			->where('option_id', $option)
			->get();
	}

	public function getProductByFilter(array $data)
	{
		$query = $this->model;

		$query = $query->whereIn('id', $data['options']);

		// $query = $query->whereHas('option', function ($query) use ($options) {
		// 	$query->where('option_value_id', $options[1]);
		// }


		// $options = $data['options'];
		// $query = $query->whereHas('option_values', function ($query) use ($options) {
		// 	$query->where('option_value_id', $options[1])
		// 		->where('option_value_id', $options[3]);
		// });

		// // filter options
		// // if (!isset($data['options']) || ! count($data['options'])) {
		// 	// $options = $data['options'];
		// 	// $query = $query->whereHas('option_values', function ($query) use ($options) {
		// 	// 	// foreach ($options as $key => $value) {
		// 	// 	// 	$q->where('option_value_id', $value);
		// 	// 	// }
		// 	// 	$query->whereIn('option_value_id', $options);
		// 	// });
		// // }

		return $query->get();
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return OptionValue
	 */
	public function create(array $data) : OptionValue
	{
		return DB::transaction(function () use ($data) {
			$OptionValue = parent::create([
				'option_id'		=> $data['option_id'],
				'name'			=> $data['name'],
			]);

			if ($OptionValue) {

				return $OptionValue;
			}

			throw new GeneralException(__('exceptions.options.create_error'));
		});
	}

	/**
	 * @param OptionValue $optionValue
	 * @param array $data
	 *
	 * @return OptionValue
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(OptionValue $optionValue, array $data) : OptionValue
	{
		return DB::transaction(function () use ($optionValue, $data) {
			if ($optionValue->update([
				'name' => $data['name']
			])) {
				return $optionValue;
			}

			throw new GeneralException(__('exceptions.option_values.update_error'));
		});
	}
}
