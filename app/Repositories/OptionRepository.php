<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Option\Option;

/**
 * Class OptionRepository.
 */
class OptionRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Option::class;
	}

	public function getOptions()
	{
		return $this->model
			->get();
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return Option
	 */
	public function create(array $data) : Option
	{
		return DB::transaction(function () use ($data) {
			$option = parent::create([
				'name'			=> $data['name'],
			]);

			if ($option) {

				return $option;
			}

			throw new GeneralException(__('exceptions.options.create_error'));
		});
	}

	/**
	 * @param Option $option
	 * @param array $data
	 *
	 * @return Option
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Option $option, array $data) : Option
	{
		return DB::transaction(function () use ($option, $data) {
			if ($option->update([
				'name' => $data['name']
			])) {
				return $option;
			}

			throw new GeneralException(__('exceptions.options.update_error'));
		});
	}
}
