<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use App\Models\Category\Category;

/**
 * Class CategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
	/**
	 * @return string
	 */
	public function model()
	{
		return Category::class;
	}

	public function getCategories(array $options = [])
	{
		$query = $this->model;

		if ($options) {

			// by ID
			if (isset($options['id'])) {
				$query = $query->where('id', $options['id']);
			}

			// by active
			if (isset($options['active'])) {
				$query = $query->where('active', $options['active']);
			}
		}

		return $query->get();
	}

	/**
	 * @param array $data
	 *
	 * @throws \Exception
	 * @throws \Throwable
	 * @return Category
	 */
	public function create(array $data) : Category
	{
		return DB::transaction(function () use ($data) {
			$category = parent::create([
				'name'			=> $data['name'],
			]);

			// if adding any option
			if (!isset($data['options']) || ! count($data['options'])) {
				$data['options'] = [];
			}

			if ($category) {

				// adding option
				$category->options()->sync($data['options']);

				return $category;
			}

			throw new GeneralException(__('exceptions.categories.create_error'));
		});
	}

	/**
	 * @param Category  $category
	 * @param array $data
	 *
	 * @return Category
	 * @throws GeneralException
	 * @throws \Exception
	 * @throws \Throwable
	 */
	public function update(Category $category, array $data) : Category
	{
		// if adding any option
		if (!isset($data['options']) || ! count($data['options'])) {
			$data['options'] = [];
		}

		return DB::transaction(function () use ($category, $data) {
			if ($category->update([
				'name' => $data['name']
			])) {

				// update option
				$category->options()->sync($data['options']);

				return $category;
			}

			throw new GeneralException(__('exceptions.categories.update_error'));
		});
	}
}
