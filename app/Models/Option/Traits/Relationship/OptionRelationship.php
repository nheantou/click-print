<?php

namespace App\Models\Option\Traits\Relationship;

use App\Models\OptionValue\OptionValue;

/**
 * Class OptionRelationship.
 */
trait OptionRelationship
{
	/**
	 * @return mixed
	 */
	public function values()
	{
		return $this->hasMany(OptionValue::class);
	}

}
