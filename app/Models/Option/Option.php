<?php

namespace App\Models\Option;

use Illuminate\Database\Eloquent\Model;
use App\Models\Option\Traits\Attribute\OptionAttribute;
use App\Models\Option\Traits\Relationship\OptionRelationship;

class Option extends Model
{
	use OptionAttribute,
		OptionRelationship;

	protected $table = 'admin_options';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'active'
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];
}
