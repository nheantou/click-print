<?php

namespace App\Models\Category\Traits\Attribute;

/**
 * Trait CategoryAttribute.
 */
trait CategoryAttribute
{

	/**
	 * @return string
	 */
	public function getIconLabelAttribute()
	{
		if ($this->icon) {
			return '<i class="' . $this->icon . '"></i>';
		}

		return '';
	}

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute()
	{
		if ($this->active) {
			return '<span class="badge badge-success">' . __('strings.general.active') . '</span>';
		}

		return '<span class="badge badge-danger">' . __('strings.general.not_active') . '</span>';
	}

	/**
	 * @return string
	 */
	public function getEditButtonAttribute()
	{
		return '<a href="' . route('admin.category.edit', $this->id). '" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
	}

	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute()
	{
		return '<a class="btn btn-danger" href="#"><i class="fas fa-trash"></i></a>';
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute()
	{
		return '
			<div class="btn-group" role="group" aria-label="' . __('labels.backend.access.users.user_actions') . '">
				' . $this->edit_button . '
				' . $this->delete_button . '
			</div>';
	}
}
