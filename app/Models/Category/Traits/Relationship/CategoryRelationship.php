<?php

namespace App\Models\Category\Traits\Relationship;

use App\Models\Option\Option;

/**
 * Class CategoryRelationship.
 */
trait CategoryRelationship
{
	/**
	 * @return mixed
	 */
	public function options()
	{
		return $this->belongsToMany(Option::class, 'admin_category_option', 'category_id', 'option_id');
	}

}
