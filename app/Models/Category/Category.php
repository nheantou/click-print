<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Model;
use App\Models\Category\Traits\Relationship\CategoryRelationship;
use App\Models\Category\Traits\Attribute\CategoryAttribute;

class Category extends Model
{
	use CategoryRelationship,
		CategoryAttribute;

	protected $table = 'admin_categories';

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
    	'icon',
    	'active'
    ];

    protected $dates = [
		'created_at',
		'updated_at',
	];
}
