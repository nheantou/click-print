<?php

namespace App\Models\OptionValue;

use Illuminate\Database\Eloquent\Model;
use App\Models\OptionValue\Traits\Attribute\OptionValueAttribute;
use App\Models\OptionValue\Traits\Relationship\OptionValueRelationship;

class OptionValue extends Model
{
	use OptionValueAttribute,
		OptionValueRelationship;

	protected $table = 'admin_option_values';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'option_id',
		'name',
		'active'
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];
}
