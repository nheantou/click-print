<?php

namespace App\Models\OptionValue\Traits\Relationship;

use App\Models\Option\Option;
use App\Models\Product\Product;

/**
 * Class OptionValueRelationship.
 */
trait OptionValueRelationship
{
	/**
	 * @return mixed
	 */
	public function option()
	{
		return $this->belongsTo(Option::class);
	}

	/**
	 * @return mixed
	 */
	public function products()
	{
		return $this->belongsToMany(Product::class, 'tran_product_option', 'option_value_id', 'product_id');
	}

}
