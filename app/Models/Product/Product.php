<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Traits\Attribute\ProductAttribute;
use App\Models\Product\Traits\Relationship\ProductRelationship;

class Product extends Model
{
	use ProductAttribute,
		ProductRelationship;

	protected $table = 'tran_products';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'code',
		'name',
		'category_id',
		'price',
		'active'
	];

	/**
	 * @var array
	 */
	protected $dates = [
		'created_at',
		'updated_at'
	];
}
