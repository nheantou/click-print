<?php

namespace App\Models\Product\Traits\Attribute;

/**
 * Trait ProductAttribute.
 */
trait ProductAttribute
{

	/**
	 * @return string
	 */
	public function getStatusLabelAttribute()
	{
		if ($this->active) {
			return "<span class='badge badge-success'>".__('labels.general.active').'</span>';
		}

		return "<span class='badge badge-danger'>".__('labels.general.inactive').'</span>';
	}

	/**
	 * @return string
	 */
	public function getPriceLabelAttribute()
	{
		return $this->price . ' ' .__('strings.general.riel');
	}

	/**
	 * @return string
	 */
	public function getEditButtonAttribute()
	{
		return '<a href="' . route('admin.product.edit', $this->id). '" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>';
	}

	/**
	 * @return string
	 */
	public function getDeleteButtonAttribute()
	{
		return '<a class="btn btn-danger" href="#"><i class="fas fa-trash"></i></a>';
	}

	/**
	 * @return string
	 */
	public function getActionButtonsAttribute()
	{
		return '
			<div class="btn-group" role="group" aria-label="' . __('labels.backend.access.users.user_actions') . '">
				' . $this->edit_button . '
				' . $this->delete_button . '
			</div>';
	}
}
