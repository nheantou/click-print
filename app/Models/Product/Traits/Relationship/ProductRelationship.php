<?php

namespace App\Models\Product\Traits\Relationship;

use App\Models\Category\Category;
use App\Models\OptionValue\OptionValue;

/**
 * Class ProductRelationship.
 */
trait ProductRelationship
{
	/**
	 * @return mixed
	 */
	public function category()
	{
		return $this->belongsTo(Category::class);
	}

	/**
	 * @return mixed
	 */
	public function option_values()
	{
		return $this->belongsToMany(OptionValue::class, 'tran_product_option', 'product_id', 'option_value_id');
	}

}
