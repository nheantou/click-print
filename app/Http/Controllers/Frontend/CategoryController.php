<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;

/**
 * Class CategoryController.
 */
class CategoryController extends Controller
{
	/**
	 * @var CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * CategoryController constructor.
	 *
	 * @param CategoryRepository $categoryRepository
	 */
	public function __construct(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{			
		return view('frontend.')
			->withCategories($this->categoryRepository);
	}

	/**
	 * ajax request category changed
	 * @return \Illuminate\View\View
	 */
	public function getOption(Category $category)
	{
		$view = '';
		if ($category) {
			// get option by category_id
			$options = $category->options;

			$view = view('frontend.options.value')
				->withOptions($options)
				->render();
		}
			
		return response()->json(['html' => $view], 200);
	}
}
