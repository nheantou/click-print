<?php

namespace App\Http\Controllers\Frontend\Sale;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;

class SaleController extends Controller
{
	public function index(CategoryRepository $categoryRepository)
	{
		$categories = $categoryRepository->getCategories(['active' => 1]);

		// // get subCategory
		// $subCategories = '';
		// if (!empty($categories)) {
		// 	$subCategories = $categoryRepository->getAllCategories($categories[0]->id);
		// }

		// get Product by category
		// $products = '';
		// if (!empty($categories)) {
		// 	$products = $subCategories[0]->subCatProducts;
		// }

		return view('frontend.sales.index')
			->withCategories($categories);
			// ->withSubCategories($subCategories)
			// ->withProducts($products);
	}

	public function payment()
	{
		return view('frontend.sales.payment');
	}

	public function invoice()
	{
		return view('frontend.sales.invoice');
	}
}
