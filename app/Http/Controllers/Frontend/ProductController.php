<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Product;
use App\Models\Category\Category;
use App\Repositories\ProductRepository;

/**
 * Class ProductController.
 */
class ProductController extends Controller
{
	   /**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 * ProductController constructor.
	 *
	 * @param ProductRepository $productRepository
	 */
	public function __construct(ProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}

	/**
	 * ajax request productByCategory
	 * @return \Illuminate\View\View
	 */
	// public function productByCategory(Category $category)
	// {
	// 	$view = '';
	// 	if ($category) {
	// 		$products = $this->productRepository->getProductByCategory($category->id);

	// 		$view = view('frontend.products.list')
	// 			->withProducts($products)
	// 			->render();
	// 	}
			
	// 	return response()->json(['html' => $view], 200);
	// }

	/**
	 * ajax request search
	 * @return \Illuminate\View\View
	 */
	public function search(Request $request)
	{
		$view = '';
		// if ($category) {
			$products = $this->productRepository->getProductByFilter($request->only(
				'category',
				'options'
			));

			$view = view('frontend.products.list')
				->withProducts($products)
				->render();
		// }
			
		return response()->json(['html' => $view], 200);
	}
}
