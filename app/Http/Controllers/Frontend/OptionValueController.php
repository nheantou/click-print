<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Option\Option;
use App\Models\OptionValue\OptionValue;
use App\Repositories\OptionValueRepository;

/**
 * Class OptionValueController.
 */
class OptionValueController extends Controller
{
	/**
	 * @var OptionValueRepository
	 */
	protected $optionValueRepository;

	/**
	 * OptionValueController constructor.
	 *
	 * @param OptionValueRepository $optionValueRepository
	 */
	public function __construct(OptionValueRepository $optionValueRepository)
	{
		$this->optionValueRepository = $optionValueRepository;
	}

	/**
	 * ajax request search
	 * @return \Illuminate\View\View
	 */
	public function search(Request $request)
	{
		$view = '';
		// if ($category) {
			$optionValues = $this->optionValueRepository->getProductByFilter($request->only(
				'category',
				'options'
			));

foreach ($optionValues as $key => $value) {
	foreach ($value->products as $p) {
	echo '<pre>';
			print_r($p->code);
			echo '</pre>';
		}
}


			// $view = view('frontend.products.list')
			// 	->withProducts($products)
			// 	->render();
		// }
			
	//	return response()->json(['html' => $view], 200);
	}
}
