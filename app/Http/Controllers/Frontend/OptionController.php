<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Option\Option;
use App\Repositories\OptionRepository;

/**
 * Class OptionController.
 */
class OptionController extends Controller
{
		/**
	 * @var OptionRepository
	 */
	protected $optionRepository;

	/**
	 * OptionController constructor.
	 *
	 * @param OptionRepository $optionRepository
	 */
	public function __construct(OptionRepository $optionRepository)
	{
		$this->optionRepository = $optionRepository;
	}
}
