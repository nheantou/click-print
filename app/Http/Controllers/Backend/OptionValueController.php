<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Option\Option;
use App\Models\OptionValue\OptionValue;
use App\Repositories\OptionValueRepository;

/**
 * Class OptionValueController.
 */
class OptionValueController extends Controller
{
	/**
	 * @var OptionValueRepository
	 */
	protected $optionValueRepository;

	/**
	 * OptionValueController constructor.
	 *
	 * @param OptionValueRepository $optionValueRepository
	 */
	public function __construct(OptionValueRepository $optionValueRepository)
	{
		$this->optionValueRepository = $optionValueRepository;
	}
	
	/**
	 * @return \Illuminate\View\View
	 */
	public function index(Option $option)
	{
		return view('backend.option_values.index')
			->withOptionId($option->id)
			->withOptionValues($this->optionValueRepository->getOptionValues($option->id));
	}

	/**
	 * @param ManageOptionValueRequest    $request
	 *
	 * @return mixed
	 */
	public function create(Request $request, Option $option)
	{
		return view('backend.option_values.create')
			->withOptionId($option->id);
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function store(Option $option, Request $request)
	{
		// added array item
		$request->merge(['option_id' => $option->id]);

		$this->optionValueRepository->create($request->only(
			'name',
			'option_id'
		));
		
		return redirect()->route('admin.option.value.index', $option->id)->withFlashSuccess(__('alerts.options.updated'));
	}

	/**
	 * @param Option $option
	 *
	 * @return mixed
	 */
	public function edit(Option $option, OptionValue $optionValue)
	{		
		return view('backend.option_values.edit')
			->withOptionId($option->id)
			->withOptionValue($optionValue);
	}

	/**
	 * @return mixed
	 * @throws \App\Exceptions\GeneralException
	 * @throws \Throwable
	 */
	public function update(Request $request, Option $option, OptionValue $optionValue)
	{
		$this->optionValueRepository->update($optionValue, $request->only(
			'name'
		));

		return redirect()->route('admin.option.value.index', $option->id)->withFlashSuccess(__('alerts.option_values.updated'));
	}
}
