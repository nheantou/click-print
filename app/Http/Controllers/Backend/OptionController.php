<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Option\Option;
use App\Repositories\OptionRepository;

/**
 * Class OptionController.
 */
class OptionController extends Controller
{
		/**
	 * @var OptionRepository
	 */
	protected $optionRepository;

	/**
	 * OptionController constructor.
	 *
	 * @param OptionRepository $optionRepository
	 */
	public function __construct(OptionRepository $optionRepository)
	{
		$this->optionRepository = $optionRepository;
	}
	
	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('backend.options.index')
			->withOptions($this->optionRepository->getOptions());
	}

	/**
	 * @param ManageCategoryRequest    $request
	 *
	 * @return mixed
	 */
	public function create(Request $request)
	{
		return view('backend.options.create');
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function store(Request $request)
	{
		$this->optionRepository->create($request->only(
			'name'
		));
		return redirect()->route('admin.option.index')->withFlashSuccess(__('alerts.options.updated'));
	}

	/**
	 * @param Option $option
	 *
	 * @return mixed
	 */
	public function edit(Option $option)
	{		
		return view('backend.options.edit')
			->withOption($option);
	}

	/**
	 * @return mixed
	 * @throws \App\Exceptions\GeneralException
	 * @throws \Throwable
	 */
	public function update(Request $request, Option $option)
	{
		$this->optionRepository->update($option, $request->only(
			'name'
		));

		return redirect()->route('admin.option.index')->withFlashSuccess(__('alerts.options.updated'));
	}
}
