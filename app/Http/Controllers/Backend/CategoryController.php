<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\OptionRepository;
use Illuminate\Http\Request;

/**
 * Class ProductController.
 */
class CategoryController extends Controller
{
	/**
	 * @var CategoryRepository
	 */
	protected $categoryRepository;

	/**
	 * CategoryController constructor.
	 *
	 * @param CategoryRepository $categoryRepository
	 */
	public function __construct(CategoryRepository $categoryRepository)
	{
		$this->categoryRepository = $categoryRepository;
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function index()
	{
		return view('backend.category.index')
			->withCategories($this->categoryRepository->getCategories());
	}

	/**
	 * @param ManageCategoryRequest    $request
	 *
	 * @return mixed
	 */
	public function create(Request $request, OptionRepository $optionRepository)
	{
		// get all options
		$options = $optionRepository->getOptions()->pluck('name', 'id');

		return view('backend.category.create')
			->withOptions($options);
	}

	/**
	 * @param StoreCategoryRequest $request
	 *
	 * @return mixed
	 * @throws \Throwable
	 */
	public function store(Request $request)
	{
		$this->categoryRepository->create($request->only(
			'name',
			'options'
		));

		return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.categories.created'));
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function edit(Category $category, OptionRepository $optionRepository)
	{
		// get all options
		$options = $optionRepository->getOptions()->pluck('name', 'id');

		return view('backend.category.edit')
			->withCategory($category)
			->withOptions($options);
	}

	/**
	 * @param UpdateCategoryRequest $request
	 * @param Category $category
	 *
	 * @return mixed
	 * @throws \App\Exceptions\GeneralException
	 * @throws \Throwable
	 */
	public function update(Request $request, Category $category)
	{
		$this->categoryRepository->update($category, $request->only(
			'name',
			'options'
		));

		return redirect()->route('admin.category.index')->withFlashSuccess(__('alerts.categories.updated'));
	}

	/**
	 * ajax request catewgory changed
	 * @return \Illuminate\View\View
	 */
	public function getOption(Category $category)
	{
		$view = '';
		if ($category) {
			// get option by category_id
			$options = $category->options;

			$view = view('backend.options.value')
				->withOptions($options)
				->render();
		}
			
		return response()->json(['html' => $view], 200);
	}
}
