<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product\Product;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use App\Repositories\OptionRepository;

/**
 * Class ProductController.
 */
class ProductController extends Controller
{
		/**
	 * @var ProductRepository
	 */
	protected $productRepository;

	/**
	 * ProductController constructor.
	 *
	 * @param ProductRepository $productRepository
	 */
	public function __construct(ProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}
	
	/**
	 * @return \Illuminate\View\View
	 */
	public function index(CategoryRepository $categoryRepository)
	{
		$products = $this->productRepository->getAllProducts();

		return view('backend.product.index')
			->withProducts($products);
	}

	/**
	 * @param ManageCategoryRequest    $request
	 *
	 * @return mixed
	 */
	public function create(Request $request, CategoryRepository $categoryRepository, OptionRepository $optionRepository)
	{
		// get all categories
		$categories = $categoryRepository->getCategories();

		// get option by category_id
		$options = $categories[0]->options;

		return view('backend.product.create')
			->withCategories($categories->pluck('name', 'id'))
			->withOptions($options);
	}

	/**
	 * @return \Illuminate\View\View
	 */
	public function store(Request $request)
	{
		$this->productRepository->create($request->only(
			'code',
			'name',
			'category',
			'price',
			'options'
		));
		return redirect()->route('admin.product.index')->withFlashSuccess(__('alerts.products.updated'));
	}

	/**
	 * @param Product $product
	 *
	 * @return mixed
	 */
	public function edit(Product $product, CategoryRepository $categoryRepository)
	{		
		// get all categories
		$categories = $categoryRepository->getCategories();

		// get option by category_id
		$options = $product->category->options;

		// set select value by each other dropdown option value
		$values = $product->option_values->mapWithKeys(function ($values) {
			return [$values->option_id => $values->id];
		});

		return view('backend.product.edit')
			->withProduct($product)
			->withCategories($categories->pluck('name', 'id'))
			->withOptions($options)
			->withValues($values);
	}

	/**
	 * @param UpdateParticipantRequest $request
	 * @param Participant              $participant
	 *
	 * @return mixed
	 * @throws \App\Exceptions\GeneralException
	 * @throws \Throwable
	 */
	public function update(Request $request, Product $product)
	{
		$this->productRepository->update($product, $request->only(
			'code',
			'name',
			'category',
			'price',
			'options'
		));

		return redirect()->route('admin.product.index')->withFlashSuccess(__('alerts.products.updated'));
	}
}
