<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\OptionController;
use App\Http\Controllers\Backend\OptionValueController;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');



// Product
Route::group(['prefix' => 'product', 'as' => 'product.'], function () {

	/*
	 * CRUD
	 */
	Route::get('/', [ProductController::class, 'index'])->name('index');
	Route::get('create', [ProductController::class, 'create'])->name('create');
	Route::post('/', [ProductController::class, 'store'])->name('store');

	/*
	 * Specific Product
	 */
	Route::group(['prefix' => '{product}'], function () {
		Route::get('edit', [ProductController::class, 'edit'])->name('edit');
		Route::patch('/', [ProductController::class, 'update'])->name('update');
	});
});

// Category
Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
	/*
	 * CRUD
	 */
	Route::get('/', [CategoryController::class, 'index'])->name('index');
	Route::get('create', [CategoryController::class, 'create'])->name('create');
	Route::post('/', [CategoryController::class, 'store'])->name('store');

	/*
	 * Specific Category
	 */
	Route::group(['prefix' => '{category}'], function () {
		Route::get('edit', [CategoryController::class, 'edit'])->name('edit');
		Route::patch('/', [CategoryController::class, 'update'])->name('update');

		// ajax
		Route::get('option', [CategoryController::class, 'getOption'])->name('option');
	});
});

// Option
Route::group(['prefix' => 'option', 'as' => 'option.'], function () {

	/*
	 * CRUD
	 */
	Route::get('/', [OptionController::class, 'index'])->name('index');
	Route::get('create', [OptionController::class, 'create'])->name('create');
	Route::post('/', [OptionController::class, 'store'])->name('store');

	/*
	 * Specific Option
	 */
	Route::group(['prefix' => '{option}'], function () {
		Route::get('edit', [OptionController::class, 'edit'])->name('edit');
		Route::patch('/', [OptionController::class, 'update'])->name('update');
	});
});

// Option Value
Route::group(['prefix' => 'option/{option}/values', 'as' => 'option.value.'], function () {

	/*
	 * CRUD
	 */
	Route::get('/', [OptionValueController::class, 'index'])->name('index');
	Route::get('create', [OptionValueController::class, 'create'])->name('create');
	Route::post('/', [OptionValueController::class, 'store'])->name('store');

	/*
	 * Specific Option
	 */
	Route::group(['prefix' => '{optionValue}'], function () {
		Route::get('edit', [OptionValueController::class, 'edit'])->name('edit');
		Route::patch('/', [OptionValueController::class, 'update'])->name('update');
	});
});

