<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\Sale\SaleController;
use App\Http\Controllers\Frontend\CategoryController;
use App\Http\Controllers\Frontend\ProductController;
use App\Http\Controllers\Frontend\OptionController;
use App\Http\Controllers\Frontend\OptionValueController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
// These routes require no user to be logged in
Route::group(['middleware' => 'guest'], function () {
	Route::get('/', [LoginController::class, 'showIdPinForm'])->name('index');
});

Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
	Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
		// User Dashboard Specific
		Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

		// User Account Specific
		Route::get('account', [AccountController::class, 'index'])->name('account');

		// User Profile Specific
		Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
	});
});

// These routes require the user to be logged in
Route::group(['middleware' => 'auth'], function () {
	// Sale
	Route::group(['prefix' => 'sale', 'namespace' => 'Sale', 'as' => 'sale.'], function () {
		Route::get('/', [SaleController::class, 'index'])->name('index');
		Route::get('payment', [SaleController::class, 'payment'])->name('payment');
		Route::get('invoice', [SaleController::class, 'invoice'])->name('invoice');
	});

	// Category
	Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
		Route::get('/', [CategoryController::class, 'index'])->name('index');

		/*
		 * Specific Category
		 */
		Route::group(['prefix' => '{category}'], function () {
			// ajax
			Route::get('option', [CategoryController::class, 'getOption'])->name('option');
		});

		// get products by category
		Route::get('{category}/products', [ProductController::class, 'productByCategory'])->name('product');
	});

	// // Option
	// Route::group(['prefix' => 'option', 'as' => 'option.'], function () {
	// 	Route::get('/{category}', [OptionController::class, 'index'])->name('index');
	// });

	// Product
	Route::group(['prefix' => 'product', 'as' => 'product.'], function () {
		Route::get('/', [ProductController::class, 'index'])->name('index');

		/*
		 * Specific Category
		 */
		Route::group(['prefix' => '{category}'], function () {
			// ajax
			Route::get('option', [CategoryController::class, 'getOption'])->name('option');
		});

		// get products by category
		Route::get('{category}/products', [ProductController::class, 'productByCategory'])->name('product');

		// ajax
		Route::get('search', [ProductController::class, 'search'])->name('search');
	});
});
