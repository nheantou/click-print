<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
	$trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

// Product
Breadcrumbs::for('admin.product.index', function ($trail) {
	$trail->push(__('Product'), route('admin.product.index'));
});
Breadcrumbs::for('admin.product.create', function ($trail) {
	$trail->push(__('Product'), route('admin.product.create'));
});
Breadcrumbs::for('admin.product.edit', function ($trail, $id) {
	$trail->push(__('Product'), route('admin.product.edit', $id));
});
Breadcrumbs::for('admin.product.paper', function ($trail) {
	$trail->push(__('Product / Paper'), route('admin.product.paper'));
});

// Category
Breadcrumbs::for('admin.category.index', function ($trail) {
	$trail->push(__('Category'), route('admin.category.index'));
});
Breadcrumbs::for('admin.category.create', function ($trail) {
	$trail->push(__('Category'), route('admin.category.create'));
});
Breadcrumbs::for('admin.category.edit', function ($trail, $id) {
	$trail->push(__('Category'), route('admin.category.edit', $id));
});

// Option
Breadcrumbs::for('admin.option.index', function ($trail) {
	$trail->push(__('Option'), route('admin.option.index'));
});
Breadcrumbs::for('admin.option.create', function ($trail) {
	$trail->push(__('Option'), route('admin.option.create'));
});
Breadcrumbs::for('admin.option.edit', function ($trail, $id) {
	$trail->push(__('Option'), route('admin.option.edit', $id));
});

// Option Value
Breadcrumbs::for('admin.option.value.index', function ($trail, $id) {
	$trail->push(__('Option'), route('admin.option.index'));
	$trail->push(__('Values'), route('admin.option.value.index', $id));
});
Breadcrumbs::for('admin.option.value.create', function ($trail, $id) {
	$trail->push(__('Option'), route('admin.option.index'));
	$trail->push(__('Values'), route('admin.option.value.create', $id));
});
Breadcrumbs::for('admin.option.value.edit', function ($trail, $id, $valueId) {
	$trail->push(__('Option'), route('admin.option.index'));
	$trail->push(__('Values'), route('admin.option.value.edit', [$id, $valueId]));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
