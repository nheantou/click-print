/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : clickprint_db

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 08/07/2019 19:02:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin_colors
-- ----------------------------
DROP TABLE IF EXISTS `admin_colors`;
CREATE TABLE `admin_colors`  (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_colors
-- ----------------------------
INSERT INTO `admin_colors` VALUES (1, 'B/W', '2019-06-30 14:20:56', '0000-00-00 00:00:00');
INSERT INTO `admin_colors` VALUES (2, 'Color', '2019-06-30 14:21:04', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for admin_papers
-- ----------------------------
DROP TABLE IF EXISTS `admin_papers`;
CREATE TABLE `admin_papers`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `active` tinyint(1) NULL DEFAULT 1,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_papers
-- ----------------------------
INSERT INTO `admin_papers` VALUES (1, 'A0', 1, '2019-06-30 14:30:14', '0000-00-00 00:00:00');
INSERT INTO `admin_papers` VALUES (2, 'A1', 2, '2019-06-30 14:30:18', '0000-00-00 00:00:00');
INSERT INTO `admin_papers` VALUES (3, 'A2', 3, '2019-06-30 14:30:20', '0000-00-00 00:00:00');
INSERT INTO `admin_papers` VALUES (4, 'A3', 4, '2019-06-30 14:30:35', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for admin_sides
-- ----------------------------
DROP TABLE IF EXISTS `admin_sides`;
CREATE TABLE `admin_sides`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin_sides
-- ----------------------------
INSERT INTO `admin_sides` VALUES (1, 'ម្ខាង', '2019-06-30 14:39:38', '0000-00-00 00:00:00');
INSERT INTO `admin_sides` VALUES (2, 'សងខាង', '2019-06-30 14:39:59', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for audits
-- ----------------------------
DROP TABLE IF EXISTS `audits`;
CREATE TABLE `audits`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `event` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `auditable_id` bigint(20) UNSIGNED NOT NULL,
  `old_values` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `new_values` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `audits_auditable_type_auditable_id_index`(`auditable_type`, `auditable_id`) USING BTREE,
  INDEX `audits_user_id_user_type_index`(`user_id`, `user_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of audits
-- ----------------------------
INSERT INTO `audits` VALUES (1, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"timezone\":null,\"last_login_at\":null,\"last_login_ip\":null}', '{\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2019-06-29 20:05:49\",\"last_login_ip\":\"::1\"}', 'http://localhost/click-print/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-06-29 20:05:50', '2019-06-29 20:05:50');
INSERT INTO `audits` VALUES (2, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost/click-print/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-06 06:03:38', '2019-07-06 06:03:38');
INSERT INTO `audits` VALUES (3, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-06-29 20:05:49\"}', '{\"last_login_at\":\"2019-07-06 06:03:39\"}', 'http://localhost/click-print/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-06 06:03:39', '2019-07-06 06:03:39');
INSERT INTO `audits` VALUES (4, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost/click-print/public/logout', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-06 06:03:47', '2019-07-06 06:03:47');
INSERT INTO `audits` VALUES (5, 'App\\Models\\Auth\\User', 3, 'updated', 'App\\Models\\Auth\\User', 3, '{\"timezone\":null,\"last_login_at\":null,\"last_login_ip\":null}', '{\"timezone\":\"America\\/New_York\",\"last_login_at\":\"2019-07-06 06:03:56\",\"last_login_ip\":\"::1\"}', 'http://localhost/click-print/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-06 06:03:56', '2019-07-06 06:03:56');
INSERT INTO `audits` VALUES (6, 'App\\Models\\Auth\\User', 3, 'updated', 'App\\Models\\Auth\\User', 3, '{\"last_login_at\":\"2019-07-06 06:03:56\"}', '{\"last_login_at\":\"2019-07-06 06:10:22\"}', 'http://localhost/click-print/public/login', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-06 06:10:22', '2019-07-06 06:10:22');
INSERT INTO `audits` VALUES (7, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-05 16:03:39\",\"last_login_ip\":\"::1\"}', '{\"last_login_at\":\"2019-07-08 01:12:23\",\"last_login_ip\":\"127.0.0.1\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-08 15:12:24', '2019-07-08 15:12:24');
INSERT INTO `audits` VALUES (8, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-08 15:20:27', '2019-07-08 15:20:27');
INSERT INTO `audits` VALUES (9, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-08 01:12:23\"}', '{\"last_login_at\":\"2019-07-08 01:30:58\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-08 15:30:58', '2019-07-08 15:30:58');
INSERT INTO `audits` VALUES (10, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 14:50:39', '2019-07-07 14:50:39');
INSERT INTO `audits` VALUES (11, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-08 15:30:58\"}', '{\"last_login_at\":\"2019-07-07 15:32:23\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 15:32:23', '2019-07-07 15:32:23');
INSERT INTO `audits` VALUES (12, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 15:41:33', '2019-07-07 15:41:33');
INSERT INTO `audits` VALUES (13, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-07 15:32:23\"}', '{\"last_login_at\":\"2019-07-07 15:42:54\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 15:42:54', '2019-07-07 15:42:54');
INSERT INTO `audits` VALUES (14, 'App\\Models\\Auth\\User', 1, 'created', 'App\\Models\\Auth\\User', 4, '[]', '{\"first_name\":\"oudom\",\"last_name\":\"oudom\",\"email\":\"oudomyornbsc@gmail.com\",\"active\":true,\"confirmed\":true,\"uuid\":\"47cdf28c-dff1-42a0-bcc7-f8f1de1d0677\"}', 'http://localhost:8000/admin/auth/user', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 15:59:03', '2019-07-07 15:59:03');
INSERT INTO `audits` VALUES (15, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 4, '{\"uid\":null}', '{\"uid\":\"7777\"}', 'http://localhost:8000/admin/auth/user/4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 16:25:22', '2019-07-07 16:25:22');
INSERT INTO `audits` VALUES (16, 'App\\Models\\Auth\\User', 1, 'created', 'App\\Models\\Auth\\User', 5, '[]', '{\"uid\":\"8888\",\"first_name\":\"Ou\",\"last_name\":\"Dom\",\"email\":\"oudom@admin.com\",\"active\":true,\"confirmed\":true,\"uuid\":\"18ec0089-ae7f-4428-843c-7588e25d5ae7\"}', 'http://localhost:8000/admin/auth/user', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 16:28:35', '2019-07-07 16:28:35');
INSERT INTO `audits` VALUES (17, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 16:54:48', '2019-07-07 16:54:48');
INSERT INTO `audits` VALUES (18, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-07 15:42:54\"}', '{\"last_login_at\":\"2019-07-07 17:10:42\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 17:10:42', '2019-07-07 17:10:42');
INSERT INTO `audits` VALUES (19, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-07 17:10:42\"}', '{\"last_login_at\":\"2019-07-07 22:28:26\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 22:28:26', '2019-07-07 22:28:26');
INSERT INTO `audits` VALUES (20, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '[]', '[]', 'http://localhost:8000/logout', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36', NULL, '2019-07-07 23:33:13', '2019-07-07 23:33:13');
INSERT INTO `audits` VALUES (21, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-07 22:28:26\"}', '{\"last_login_at\":\"2019-07-07 23:33:46\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, '2019-07-07 23:33:47', '2019-07-07 23:33:47');
INSERT INTO `audits` VALUES (22, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-07 23:33:46\"}', '{\"last_login_at\":\"2019-07-08 08:32:55\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, '2019-07-08 08:32:55', '2019-07-08 08:32:55');
INSERT INTO `audits` VALUES (23, 'App\\Models\\Auth\\User', 1, 'updated', 'App\\Models\\Auth\\User', 1, '{\"last_login_at\":\"2019-07-08 08:32:55\"}', '{\"last_login_at\":\"2019-07-08 16:18:36\"}', 'http://localhost:8000/admin/login', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0', NULL, '2019-07-08 16:18:37', '2019-07-08 16:18:37');

-- ----------------------------
-- Table structure for cache
-- ----------------------------
DROP TABLE IF EXISTS `cache`;
CREATE TABLE `cache`  (
  `key` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL,
  UNIQUE INDEX `cache_key_unique`(`key`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jobs
-- ----------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED NULL DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jobs_queue_index`(`queue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_09_03_144628_create_permission_tables', 1);
INSERT INTO `migrations` VALUES (4, '2017_09_11_174816_create_social_accounts_table', 1);
INSERT INTO `migrations` VALUES (5, '2017_09_26_140332_create_cache_table', 1);
INSERT INTO `migrations` VALUES (6, '2017_09_26_140528_create_sessions_table', 1);
INSERT INTO `migrations` VALUES (7, '2017_09_26_140609_create_jobs_table', 1);
INSERT INTO `migrations` VALUES (8, '2018_04_08_033256_create_password_histories_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_03_26_000344_create_audits_table', 1);

-- ----------------------------
-- Table structure for model_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE `model_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_permissions_model_type_model_id_index`(`model_type`, `model_id`) USING BTREE,
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_permissions
-- ----------------------------
INSERT INTO `model_has_permissions` VALUES (1, 'App\\Models\\Auth\\User', 4);
INSERT INTO `model_has_permissions` VALUES (1, 'App\\Models\\Auth\\User', 5);

-- ----------------------------
-- Table structure for model_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE `model_has_roles`  (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`, `model_id`, `model_type`) USING BTREE,
  INDEX `model_has_roles_model_type_model_id_index`(`model_type`, `model_id`) USING BTREE,
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of model_has_roles
-- ----------------------------
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\Auth\\User', 1);
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\Auth\\User', 4);
INSERT INTO `model_has_roles` VALUES (1, 'App\\Models\\Auth\\User', 5);
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\Auth\\User', 2);
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\Auth\\User', 4);
INSERT INTO `model_has_roles` VALUES (2, 'App\\Models\\Auth\\User', 5);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\Auth\\User', 3);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\Auth\\User', 4);
INSERT INTO `model_has_roles` VALUES (3, 'App\\Models\\Auth\\User', 5);

-- ----------------------------
-- Table structure for password_histories
-- ----------------------------
DROP TABLE IF EXISTS `password_histories`;
CREATE TABLE `password_histories`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `password_histories_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `password_histories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_histories
-- ----------------------------
INSERT INTO `password_histories` VALUES (1, 1, '$2y$10$9iz55I58GZoKjtMCyeRpBukkSOvy/rNvp4qF/SeFhEx8aSLrpJwAm', '2019-06-28 08:03:24', '2019-06-28 08:03:24');
INSERT INTO `password_histories` VALUES (2, 2, '$2y$10$HOWkYHZBnu8evX4HiWOr0.SRU3BV.j6/VjhDsZscGbwwRVlFaJSte', '2019-06-28 08:03:24', '2019-06-28 08:03:24');
INSERT INTO `password_histories` VALUES (3, 3, '$2y$10$V6Ho2CuguozGT5HP3xo1Ou6900.QCniMKxE/bxVuvS7MpEiAJFUAK', '2019-06-28 08:03:24', '2019-06-28 08:03:24');
INSERT INTO `password_histories` VALUES (4, 4, '$2y$10$cUv7xAs.g/YXcJbpsNRSY.cUC/Cj4HDvVehzRevW2YV0VZY7ONaMG', '2019-07-07 15:59:03', '2019-07-07 15:59:03');
INSERT INTO `password_histories` VALUES (5, 5, '$2y$10$iP/YvSFMjhEeODUxA0z68Of2AdWZfE/y8LAskesyvd.tfev2LY1gG', '2019-07-07 16:28:35', '2019-07-07 16:28:35');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for permissions
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permissions
-- ----------------------------
INSERT INTO `permissions` VALUES (1, 'view backend', 'web', '2019-06-28 08:03:25', '2019-06-28 08:03:25');

-- ----------------------------
-- Table structure for role_has_permissions
-- ----------------------------
DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE `role_has_permissions`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_has_permissions_role_id_foreign`(`role_id`) USING BTREE,
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_has_permissions
-- ----------------------------
INSERT INTO `role_has_permissions` VALUES (1, 1);
INSERT INTO `role_has_permissions` VALUES (1, 2);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `roles_name_index`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'administrator', 'web', '2019-06-28 08:03:25', '2019-06-28 08:03:25');
INSERT INTO `roles` VALUES (2, 'executive', 'web', '2019-06-28 08:03:25', '2019-06-28 08:03:25');
INSERT INTO `roles` VALUES (3, 'user', 'web', '2019-06-28 08:03:25', '2019-06-28 08:03:25');

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for social_accounts
-- ----------------------------
DROP TABLE IF EXISTS `social_accounts`;
CREATE TABLE `social_accounts`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `social_accounts_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `uid` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'gravatar',
  `avatar_location` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password_changed_at` timestamp(0) NULL DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `confirmation_code` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT 0,
  `timezone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `last_login_at` timestamp(0) NULL DEFAULT NULL,
  `last_login_ip` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'd2296cff-5627-4013-95df-27fca5a93b0f', 'Admin', 'Istrator', NULL, 'admin@admin.com', 'gravatar', NULL, '$2y$10$9iz55I58GZoKjtMCyeRpBukkSOvy/rNvp4qF/SeFhEx8aSLrpJwAm', NULL, 1, '1d0421dd60f3a949cf0e493e5661be2d', 1, 'America/New_York', '2019-07-08 16:18:36', '127.0.0.1', 0, 'mJWNKAjWAGn0QHqSw7hwnctocQ3U20FPWFNA8Dl5EMcfOSlk0xTI9zBkrbhw', '2019-06-28 08:03:24', '2019-07-08 16:18:37', NULL);
INSERT INTO `users` VALUES (2, '5f2e4847-6b5b-4cad-b921-248daae75f34', 'Backend', 'User', NULL, 'executive@executive.com', 'gravatar', NULL, '$2y$10$HOWkYHZBnu8evX4HiWOr0.SRU3BV.j6/VjhDsZscGbwwRVlFaJSte', NULL, 1, '6a4bb2184d56309f48746ed755c1bf8d', 1, NULL, NULL, NULL, 0, NULL, '2019-06-28 08:03:24', '2019-06-28 08:03:24', NULL);
INSERT INTO `users` VALUES (3, '6b839f2b-3aea-4c5b-879c-3f01dc04fe4f', 'Default', 'User', '2727', 'user@user.com', 'gravatar', NULL, '$2y$10$V6Ho2CuguozGT5HP3xo1Ou6900.QCniMKxE/bxVuvS7MpEiAJFUAK', NULL, 1, '864adf499990d58b71f376d0cb8b2548', 1, 'America/New_York', '2019-07-06 06:10:22', '::1', 0, NULL, '2019-06-28 08:03:24', '2019-07-06 06:10:22', NULL);
INSERT INTO `users` VALUES (4, '47cdf28c-dff1-42a0-bcc7-f8f1de1d0677', 'oudom', 'oudom', '7777', 'oudomyornbsc@gmail.com', 'gravatar', NULL, '$2y$10$cUv7xAs.g/YXcJbpsNRSY.cUC/Cj4HDvVehzRevW2YV0VZY7ONaMG', NULL, 1, 'c424d4ca858512496d1952be61864870', 1, NULL, NULL, NULL, 0, NULL, '2019-07-07 15:59:03', '2019-07-07 16:25:22', NULL);
INSERT INTO `users` VALUES (5, '18ec0089-ae7f-4428-843c-7588e25d5ae7', 'Ou', 'Dom', '8888', 'oudom@admin.com', 'gravatar', NULL, '$2y$10$iP/YvSFMjhEeODUxA0z68Of2AdWZfE/y8LAskesyvd.tfev2LY1gG', NULL, 1, 'b8ad37379ad4929ebffbb9f43dbfe61c', 1, NULL, NULL, NULL, 0, NULL, '2019-07-07 16:28:35', '2019-07-07 16:28:35', NULL);

SET FOREIGN_KEY_CHECKS = 1;
