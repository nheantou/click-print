-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2019 at 09:56 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `print_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_colors`
--

CREATE TABLE `admin_colors` (
  `id` int(2) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin_colors`
--

INSERT INTO `admin_colors` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'B/W', '2019-06-30 07:20:56', '0000-00-00 00:00:00'),
(2, 'Color', '2019-06-30 07:21:04', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_papers`
--

CREATE TABLE `admin_papers` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin_papers`
--

INSERT INTO `admin_papers` (`id`, `name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'A0', 1, '2019-06-30 07:30:14', '0000-00-00 00:00:00'),
(2, 'A1', 2, '2019-06-30 07:30:18', '0000-00-00 00:00:00'),
(3, 'A2', 3, '2019-06-30 07:30:20', '0000-00-00 00:00:00'),
(4, 'A3', 4, '2019-06-30 07:30:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_sides`
--

CREATE TABLE `admin_sides` (
  `id` int(11) NOT NULL,
  `name` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin_sides`
--

INSERT INTO `admin_sides` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ម្ខាង', '2019-06-30 07:39:38', '0000-00-00 00:00:00'),
(2, 'សងខាង', '2019-06-30 07:39:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

CREATE TABLE `admin_types` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`id`, `name`, `order`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Woodfree', 1, 1, '2019-06-30 07:32:40', '0000-00-00 00:00:00'),
(2, 'Glossy', 2, 1, '2019-06-30 07:32:50', '0000-00-00 00:00:00'),
(3, 'Photo Paper', 3, 1, '2019-06-30 07:33:01', '0000-00-00 00:00:00'),
(4, 'Color Paper', 4, 1, '2019-06-30 07:33:10', '0000-00-00 00:00:00'),
(5, '80gsm', 5, 1, '2019-06-30 07:33:22', '0000-00-00 00:00:00'),
(6, '100gsm', 6, 1, '2019-06-30 07:33:43', '0000-00-00 00:00:00'),
(7, '150gsm', 7, 1, '2019-06-30 07:33:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `paper_id` int(11) DEFAULT NULL,
  `gram_id` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `paper_id`, `gram_id`, `quantity`, `cost`) VALUES
(1, 1, 1, 1, '100.00'),
(2, 1, 2, 1, '200.00'),
(3, 1, 3, 1, '300.00'),
(4, 1, 4, 1, '400.00'),
(5, 1, 5, 1, '500.00'),
(6, 2, 1, 1, '100.00'),
(7, 2, 2, 1, '200.00'),
(8, 2, 3, 1, '300.00'),
(9, 2, 4, 1, '400.00'),
(10, 2, 5, 1, '500.00'),
(11, 3, 1, 1, '100.00'),
(12, 3, 2, 1, '200.00'),
(13, 3, 3, 1, '300.00'),
(14, 3, 4, 1, '400.00'),
(15, 3, 5, 1, '500.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_colors`
--
ALTER TABLE `admin_colors`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin_papers`
--
ALTER TABLE `admin_papers`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin_sides`
--
ALTER TABLE `admin_sides`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `admin_types`
--
ALTER TABLE `admin_types`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_colors`
--
ALTER TABLE `admin_colors`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_papers`
--
ALTER TABLE `admin_papers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_sides`
--
ALTER TABLE `admin_sides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admin_types`
--
ALTER TABLE `admin_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
